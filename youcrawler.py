from __future__ import print_function

import html
import json
import os
import re

import bs4
import requests
from peewee import *

IMAGES_DIR = 'images'
THUMBNAILS_DIR = 'thumbnails'
NEEDED_INFO = ('video_id', 'title', 'length_seconds', 'view_count')

db = SqliteDatabase('people.db')


def initialize():
    # Make sure DB and table exist.
    db.connect()
    db.create_tables([Video], safe=True)
    db.close()

    # Make sure images and thumbnails directories exist.
    if not os.path.exists(IMAGES_DIR):
        os.mkdir(IMAGES_DIR)

    if not os.path.exists(THUMBNAILS_DIR):
        os.mkdir(THUMBNAILS_DIR)


# ****************** Models ******************
class Video(Model):
    video_id = CharField(index=True, unique=True)
    title = CharField()
    length = IntegerField()
    views = IntegerField(default=0)
    image_path = CharField()
    thumbnail_path = CharField()

    @classmethod
    def from_info(cls, video_info):
        return cls(video_id=video_info.get('video_id'),
                   title=video_info.get('title'),
                   length=video_info.get('length_seconds'),
                   views=video_info.get('view_count'))

    @property
    def url(self):
        return "https://www.youtube.com/watch?v={vid}" \
            .format(vid=self.video_id)

    @property
    def duration(self):
        length = self.length

        seconds = length % 60
        minutes = (length // 60) % 60
        hours = ((length // 60) // 60)

        return "{0}:{1}:{2}".format(hours, minutes, seconds)

    @property
    def image_url(self):
        return "https://i.ytimg.com/vi/{vid}/hqdefault.jpg" \
            .format(vid=self.video_id)

    @property
    def thumbnail_url(self):
        return "https://i.ytimg.com/vi/{vid}/default.jpg" \
            .format(vid=self.video_id)

    class Meta:
        database = db


# ***************** Crawlers *****************
class Crawler:
    PATTERN_LOAD_MORE = re.compile(r"(/browse_ajax\?[^\"]+)")
    PATTERN_VIDEO_INFO = re.compile(r"ytplayer.config = ({.+?});")

    def __init__(self, url, video_needed_info):
        self._url = url
        self.video_needed_info = video_needed_info
        self.session = requests.session()

    def parse_video(self, video_html):
        s = self.PATTERN_VIDEO_INFO.search(video_html)

        if not s:
            return None

        youtube_player_json = s.group(1)
        args = json.loads(youtube_player_json).get('args', None)

        return {key: args.get(key, None) for key in self.video_needed_info}

    def load_more_url(self, text):
        s = self.PATTERN_LOAD_MORE.search(text)

        if not s:
            return None

        return s.group(1)

    def load_more(self, page_html):
        escaped_url = self.load_more_url(page_html)

        if not escaped_url:
            raise StopIteration

        # print('*' * 5, escaped_url)

        url = "https://www.youtube.com{ajax_part}" \
            .format(ajax_part=html.unescape(escaped_url))

        ajax_response = self.session.get(url)
        ajax_json = ajax_response.json()

        yield from self.parse_ajax_json(ajax_json)
        yield from self.load_more(ajax_json.get("load_more_widget_html", None))

    def videos(self):
        session = self.session
        page_html = session.get(self.url).text

        for video_id in self.video_ids(page_html):
            video_url = "https://www.youtube.com/watch?v={vid}".format(vid=video_id)
            video_html = session.get(video_url).text
            yield self.parse_video(video_html)

    @property
    def url(self):
        raise NotImplementedError()

    def video_ids(self, page_html):
        raise NotImplementedError()

    def parse_ajax_json(self, ajax_json):
        raise NotImplementedError()


class ChannelCrawler(Crawler):
    def video_ids(self, channel_html):
        if not channel_html:
            raise StopIteration

        soup = bs4.BeautifulSoup(channel_html, "html.parser")
        ul = soup.find("ul", {"id": "channels-browse-content-grid"})

        for li in ul.find_all("li", recursive=False):
            div = li.find('div')
            yield div.attrs.get('data-context-item-id', None)

        yield from self.load_more(channel_html)

    def parse_ajax_json(self, ajax_json):
        content_html = ajax_json.get("content_html", None)

        if not content_html:
            raise StopIteration

        soup = bs4.BeautifulSoup(content_html, "html.parser")

        for li in soup.find_all("li", recursive=False):
            div = li.find('div')
            yield div.attrs.get('data-context-item-id', None)

    @property
    def url(self):
        return self._url


class PlaylistCrawler(Crawler):
    def video_ids(self, playlist_html):
        if not playlist_html:
            raise StopIteration

        soup = bs4.BeautifulSoup(playlist_html, "html.parser")
        table = soup.find("table", {"id": "pl-video-table"})
        tbody = table.find("tbody")

        for tr in tbody.find_all("tr"):
            yield tr.attrs.get('data-video-id', None)

        yield from self.load_more(playlist_html)

    def parse_ajax_json(self, ajax_json):
        content_html = ajax_json.get("content_html", None)

        if not content_html:
            raise StopIteration

        soup = bs4.BeautifulSoup(content_html, "html.parser")

        for tr in soup.find_all("tr"):
            yield tr.attrs.get('data-video-id', None)

    @property
    def url(self):
        list_id = self._url.split("list=")[1]
        return "https://www.youtube.com/playlist?list={list_id}".format(list_id=list_id)


class VideoManager:
    def __init__(self, url):
        self.url = url

    def _create_video(self, video_info):
        if video_info and all([video_info.get(k) for k in NEEDED_INFO]):
            video = Video.from_info(video_info)
            self._download_images(video)
            video.save()

    def _download_images(self, video):
        image_path = os.path.join(IMAGES_DIR, "{0}.jpg".format(video.video_id))
        thumbnail_path = os.path.join(THUMBNAILS_DIR, "{0}.jpg".format(video.video_id))

        with open(image_path, 'wb') as f:
            r = requests.get(video.image_url)
            f.write(r.content)

        with open(thumbnail_path, 'wb') as f:
            r = requests.get(video.thumbnail_url)
            f.write(r.content)

        video.image_path = image_path
        video.thumbnail_path = thumbnail_path

    def _crawl(self, crawler):
        for video_info in crawler.videos():
            self._create_video(video_info)

    def sync_database(self, url_type):
        crawler = None

        if url_type == 'channel':
            crawler = ChannelCrawler

        if url_type == 'playlist':
            crawler = PlaylistCrawler

        if not crawler:
            raise TypeError("Only channel and playlist are supported")

        self._crawl(crawler(self.url, NEEDED_INFO))


def main():
    # url = 'https://www.youtube.com/watch?v=wWmMuc5E_ao&list=PLu_matf0Kt_Lm8Al8rz1DClRqf1LA-_X-&index=1&t=320s'
    # url_type = 'playlist'
    #
    # url = 'https://www.youtube.com/watch?v=wWmMuc5E_ao&t=4s&list=PLu_matf0Kt_Lm8Al8rz1DClRqf1LA-_X-&index=1'
    url = 'https://www.youtube.com/watch?v=upgAxjEZ7YU&index=1&list=PLfeJT8wCesumA5kQvGS4SsFVopuuFAFQ8'
    url_type = 'playlist'

    initialize()
    video_manager = VideoManager(url)
    video_manager.sync_database(url_type)


if __name__ == '__main__':
    main()
