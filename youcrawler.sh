#!/usr/bin/bash

# ***** Please set these data *****
# url to be crawled.
url=''

# channel/playlist.
type=''

# Activate inmobly-challenge env.
# workon inmobly-challenge

# Run youcrawler.py
/home/mohaned/.virtualenvs/inmobly-challenge/bin/python3.6 youcrawler.py ${type} ${url}
